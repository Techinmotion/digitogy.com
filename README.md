# Tech to Look Forward to in 2021 #

While 2020 is looking like a year that most of us want to forget, 2021 is looking like it will bring many technologies another step forward. 
There are quite a few new techs to expect next year, but there are two in particular that will interest many. 

### Self-Driving Cars ###

Many of you would have heard about the possibility of self-driving cars over the past few years. Well, 2021 is said by many to be the year when we will finally get them. Next year is when this technology is said to be made available for consumer adoption. We imagine there will be quite a competitive market with the likes of Google, Volvo, Daimler, Tesla, and Apple all producing autonomous cars. 


### 5G Devices ###

5G is already upon us, but it is the latest devices that can use it we are interested in. All the big names including Apple, Samsung, Nokia, Motorola, and more will bring out their latest range of 5G ready smartphones, tablets, watches, and everything else.

### Summary ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

They are just two expected technologies to trend in 2021, but as you can tell from the many tech reviews online, it could be a big year for other products too. Folding PC’s look set to become a big thing while virtual reality is set to be adopted on the widest scale yet. We are pretty sure you are just as excited as we are to see the start of 2021.

[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)